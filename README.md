# README #

### What is this repository for? ###

These are the examples used in my RxJS presentation.

### How do I get set up? ###

1. navigate to folder where you want the code to go
2. open command prompt
3. enter `git clone https://bitbucket.org/daryl0313/rxjs-presentation-demo`
4. enter `cd rxjs-presentation-demo`
5. enter `npm install`
6. enter `npm start`

### How to view examples? ###

These examples are very primative as there is no UI elements to the project (Outside of a textbox and some generated divs in the loader example).  Outside of one example, all output is logged to the console so make sure the developer tools window is open when running any examples.

There is only one page which runs and only has main.js attached to it.  To run an example just copy and paste the example in main.ts and let the code compile and the browser refresh.