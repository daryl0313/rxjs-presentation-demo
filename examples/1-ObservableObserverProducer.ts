import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Observer";
import { Subscription } from "rxjs/Subscription";


const producer = [
    "here is message one",
    "and message two",
    "last but not least message three"
];

// const observable = Observable.create(observer => {
const observable = new Observable(observer => {
    observer.next(producer[0]);
    observer.next(producer[1]);
    observer.next(producer[2]);
    observer.complete();
});

const observer: Observer<number> = {
    next: (value: any) => console.log(`value: ${value}`),
    error: (e) => console.log(`error: ${e}`),
    complete: () => console.log("complete")
};

let subscription: Subscription = observable.subscribe(observer);
// subscription.unsubscribe();