import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Observer";
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publish';
import 'rxjs/add/operator/take';

const source = getConnectableObservable();

source.connect();
// setTimeout(() => source.connect(), 3000);

let n = '';
for (var i = 0; i < 3; i++) {
    n += '1';
    const name = ((i + 1) * parseInt(n)).toString();
    const observer = getObserver(name);
    setTimeout(() => {
        source.subscribe(observer);
    }, 1500 * i);
}

function getConnectableObservable() {
    const result = Observable.interval(1000)
        .take(5)
        .publish();
    return result;
}

function getObserver(name: string): Observer<number> {
    return {
        next: (value: any) => console.log(name, value),
        error: (e) => console.log(name, e),
        complete: () => console.log(name, "complete")
    }
}
