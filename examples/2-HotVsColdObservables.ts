import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Observer";
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/defer';

// const source = Observable.fromPromise(getPromise());
const source = Observable.defer(() => Observable.fromPromise(getPromise()));

console.log('before subscribe');

source.subscribe(
    (value: any) => console.log(`value: ${value}`),
    (e) => console.log(`error: ${e}`),
    () => console.log("complete")
);

function getPromise() {
    return new Promise(resolve => {
        console.log('Promise executed!');
        resolve(42);
    });
}
