import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import "rxjs/add/operator/map";
import "rxjs/add/operator/throttle";
import "rxjs/add/operator/debounce";
import "rxjs/add/observable/interval";
import "rxjs/add/observable/fromEvent";

const textbox = <HTMLInputElement>document.getElementById("input");

const source = new Subject<string>();
textbox.onkeyup = (ev: Event) => {
    source.next(textbox.value);
};

// const source = Observable.fromEvent(textbox, "keyup").map(() => textbox.value);

source
    // .debounce(() => Observable.interval(1000))
    // .throttle(() => Observable.interval(1000))
    .subscribe(value => console.log("Emitted Value", value));
