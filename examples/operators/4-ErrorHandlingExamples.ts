import { Observable } from "rxjs";
import { load, loadWithFetch } from "../../loader";

const output = document.getElementById('output');

const source = Observable.create(observer => {
    observer.next(1);
    observer.next(2);
    // observer.error('Stop!');
    // throw new Error('Stop!');

    observer.next(3);
    observer.complete();
})
    .catch(e => {
        console.log(`caught: ${e}`);
        return Observable.of(10);
    });

// error example
source.subscribe(
    value => console.log(`value: ${value}`),
    error => console.log(`error: ${error}`),
    () => console.log('complete')
);

// retry example
// const sub = load('movies.json').subscribe(
//     renderMovies,
//     e => console.log(`error: ${e}`),
//     () => console.log('complete')
// );

// setTimeout(() => sub.unsubscribe(), 1000);

function renderMovies(movies) {
    movies.forEach(m => {
        const div = document.createElement('div');
        div.innerText = m.title;
        output.appendChild(div);
    })
}