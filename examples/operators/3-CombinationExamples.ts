import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/take";
import "rxjs/add/operator/switch";
import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/zip";
import "rxjs/add/operator/concat";
import "rxjs/add/operator/combineLatest";
import "rxjs/add/operator/merge";
import "rxjs/add/observable/merge";
import "rxjs/add/observable/interval";
import "rxjs/add/observable/range";

const firstproducer = [1, 3, 6, 4, 9];
const secondProducer = ["val1", "val2", "val3"];

const observable1 = new Observable<number>(observer => {
    for (let i = 0; i < firstproducer.length; i++) {
        observer.next(firstproducer[i]);
    }
    observer.complete();
});
const observable2 = new Observable<string>(observer => {
    for (let v of secondProducer) {
        observer.next(v);
    }
    observer.complete();
});

// merge http://rxmarbles.com/#merge
Observable.merge(observable1, observable2).subscribe(getObserver("merge"));
// observable1.merge(observable2).subscribe(getObserver("merge"));

// // concat http://rxmarbles.com/#concat
// observable1.concat(observable2).subscribe(getObserver("concat"));

// // combineLatest
// observable1.combineLatest(observable2).subscribe(getObserver("combineLatest"));

// // zip
// observable1.zip(observable2).subscribe(getObserver("zip"));

// // switch
// observable1.map(v => Observable.range(v, 3)).switch().subscribe(getObserver("switch"));

// // switchMap
// observable1.switchMap(v => Observable.range(v, 3)).subscribe(getObserver("switchMap"));



// // another switch example
// const interval$ = Observable.interval(1000).take(5);
// const intervalSub = interval$.subscribe(getObserver('switch (interval)'));
// const sub2 = interval$.switchMap(i => observable1).subscribe(getObserver("switch"));
// // setTimeout(() => sub2.unsubscribe(), 3000);


function getObserver(name: string) {
    return {
        next: (value: any) => console.log(name, value),
        error: (e) => console.log(`ERROR: ${e} ---------------------`),
        complete: () => console.log(`---------- ${name} complete ----------`)
    };
}
