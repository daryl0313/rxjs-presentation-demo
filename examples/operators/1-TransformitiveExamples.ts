import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/from";
import "rxjs/add/operator/take";
import "rxjs/add/operator/toArray";
import "rxjs/add/operator/map";
import "rxjs/add/operator/first";
import "rxjs/add/operator/groupBy";
import "rxjs/add/operator/reduce";
import "rxjs/add/operator/mergeMap";

const producer = [
    { prop1: "asdf", arrayValues: [{ arrayName: "item 1" }] },
    { prop1: "qwerty", arrayValues: [{ arrayName: "item one" }, { arrayName: "item two" }] },
    { prop1: "asdf", arrayValues: [{ arrayName: "first item" }, { arrayName: "second item" }] }
];

const source = Observable.from(producer);

// map
source.map(v => v.prop1).subscribe(getObserver("map observable"));

// flatmap
source.flatMap(v => v.arrayValues).subscribe(getObserver("flatMap observable"));

// groupby
source.groupBy(v => v.prop1)
    .flatMap(group => group.reduce((acc: any[], value: any, i: number) => [...acc, value], []))
    .subscribe(getObserver("groupBy observable"));

// first
source.first().subscribe(getObserver("first observable"));

// take
source.take(2).subscribe(getObserver("take observable"));




function getObserver(name: string) {
    return {
        next: (value: any) => console.log(name, value),
        error: (e) => console.log(`error: ${e}`),
        complete: () => console.log(`---------- ${name} complete ----------`)
    };
}